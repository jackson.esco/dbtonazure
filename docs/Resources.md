# Resources

[[_TOC_]]

## Introduction
The following resources gets deployed as part of the installation.

| Resource Name | Comment |
| :---------- | :--- |
| dbtonaz_rg | Resource group onto which this prototype implementation is deployed. |
| hmuse1dbtazuai01 | User assigned identity with which we use to connect to different resources like Key vault, and retrieve secrets etc.. |
| hmuse1dbtazsta01 | This will house the Azure file share where the dbt project code will be archived and stored. |
| hmuse1dbtazkv01 | This will host specific secrets, which contain the data warehouse connection information. |
| hmuse1dbtazacr01 | Used to host the customer built docker image. |
| hmuse1dbtazaci01 | The container group / instance that will run the dbt pipeline. |
| hmuse1dbtazadf01 | Azure data factory with a preconfigured pipeline of start the ACG and wait for its completion. | 

## Key vault
The following secrets gets configured in the key vault (hmuse1dbtazkv01).
![](./images/kv_secrets.png)

The datawarehouse (ex: snowflake connection) is stored in a custom json format.
![](./images/secrets_connection_info.png)

The key vault is configured with appropriate access policies. In the below we demonstrate that the user managed identity (hmuse1dbtazuai01) has the read permission for the secrets.

![](./images/keyvault_access_policies.png)

## Storage Account 
The DBT project, for ex: [../dbtdataops/dbtoncloud](../dbtdataops/dbtoncloud), could be mounted to the Azure Container Instance (ACI) via [Git](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-volume-gitrepo). I have choosen to avoid it for cases in scenarios like if the client is not using Git.

The dbt project is archived into zip format:
![](./images/dbtcloud_zip_content.png)

This is then uploaded to the Azure file share (codeshare) which is configured in the storage account (hmuse1dbtazsta01).
![](./images/azfileshare_dbt_project.png)

 This file share will be mounted in the ACI at mount '/code/'. [Doc: Mount an Azure file share in Azure Container Instances](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-volume-azure-files)

 ![](./images/aci_configured_properties.png)


## User Managed Identity 
During ACI initialization we need to retrieve secrets, for ex connection info, from key vaults. In order for the container to access we define an user managed identity,hmuse1dbtazuai01. We then assign the access policy on key vault.

![](./images/keyvault_access_policies.png)

The user managed identity is assigned to the ACI as part of the deployment.
![](./images/aci_uai_identity.png)

## Azure Container Repository
We are storing the custom built image in Azure Container Registry (hmuse1dbtazacr01)
to utilize some of its potential feature like cached image.

![](./images/acr.png)

## Azure Container Instance
For each dbt final model that gets materialized by DBT, we need to create a specific
ACI. For the prototype though I am not actually creating dbt model; i am trying to 
demonstrate the solution leading up to that point.

We start of configuring the various environment variables required for a successfull run.

| Environment variable | Description |
| :-- | :-- |
| ENV_KV_URL | The key vault url, which host the connection secret. |
| ENV_KV_SFLK | The key vault secret |
| ENV_DBT_PACKAGED | The path to the dbt packed artifact in the azure file share. |
| ENV_DBT_RUN_CMD | The path to the data pipeline script which will effectively run the dbt run command. This path is the path to the file inside the project archive. |
| ENV_AZSUB | Current azure subscription, needed during runtime |
| ENV_RG | The resource group where the ACI is hosted. |


![](./images/aci_environment.png)

### Do we need a public ip address & port ?
Ideally we do not need a public ip address for the ACI, however due to
current terraform azure provider implementation bug, we have to assign it.
hence every time the ACI gets instantiated a random public ip address will be 
assigned. The port for now UDP (9999). No service is runnning at this port. 

### What are the configurations we need ?
Since the actual work is done by the datawarehouse, we need only a bare min configuration; 
which is 1 vCPU & 1 GB mem, 0.5GB is better.

### Where are the docker scripts present ?
You would find the docker source artifacts in the directory [../dbtdocker](../dbtdocker)

## Azure Data Factory (ADF)
To demonstrate how to trigger the ACG from ADF (hmuse1dbtazadf01), this instance was created. 
its preconfigured to read service principal information from the key vault via a linked service:

![](./images/adf_kv_ls.png)

And also a parameterized pipeline, acg_start_and_wait_pipe :
![](./images/acg_start_and_wait_pipe.png)

