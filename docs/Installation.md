# Installation

This page walks through how to install the demo code that has been supplied here and its the outcome. It is assumed that you would know Terraform to perform the below deployment.

## Target deployment
This reference prototype installation when deployed would create different resources as below:

![./images/dbtonaz_rg.png](./images/dbtonaz_rg.png)

### Video Walk thru
Video walk thru : [videos/tf_deployment.mp4](./videos/tf_deployment.mp4)

The above walk thru highlights
 - setting up base parameters using the [../bin/azlogin.sh](../bin/azlogin.sh).
 - Terraform initialization
 - Terraform apply
 - The key vault secrets getting stored with the dummy snowflake connection.
 - The dbt project packaged and stored in azure file share
 - The custom docker stored in ACR
 - The executed ACI instance.

It is possible due to timeout issue, ACG might not get deployed at first attempt,
you might have to re-apply terraform again to get it installed.

### Setting up workspace
We are going to using Terraform scripts found in [../iac](../iac) folder. 
For the prototype, i am using a service principal which has appropriate rights
to create resource group, role assignment on the subscription.

You would need to start of with configure the appropriate configuration in the 
base script [../bin/azlogin.sh](../bin/azlogin.sh). Once configured, here are 
the steps:

```shell
  cd iac
  . ../bin/azlogin.sh
  terraform init
  terraform apply
```

### Variable
Update the [../iac/variables.tf](../iac/variables.tf) with the appropriate name
and or Azure region as you see fit.
