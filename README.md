
# Deploying and Running DBT on Azure container Instance

*Dated : Dec-2020*

*Cost-Effective approach to executing data transformations using DBT in Azure.*

## Links
 - [Deep understanding into the prototype](./docs/Overview.md)
- [Trigger by ADF](./docs/ADF_Trigger.md)

## Overview
[DBT](www.getdbt.com) has been a widely adopted tool for data transformation pipelines. We have 
been recommending and implementing in multiple of our client engagements. While DBT itself is a 
python based framework, however you would need to run it using DBT cli commands. This means that 
it requires an execution environment, preferably a python virtual environment, running in a VM.

There have been a number of approaches discussed to achieve this including:
- Automate Code Deployment with AWS EC2 Build Agents
- [Running dbt in Production](https://docs.getdbt.com/docs/running-a-dbt-project/running-dbt-in-production/)
- [Dbt Deployment in Secure Environments](https://discourse.getdbt.com/t/deployment-in-secure-environments/573)
- [Best Practices for CICD Deployment](https://discourse.getdbt.com/t/best-practices-for-cicd-deployment/618)

And of course, there is the option of [dbt Cloud](https://blog.getdbt.com/introducing-dbt-cloud/) itself. But 
what if you had different criteria?
- Offer a multi-tenant dbt cluster - an environment where multiple independent dbt pipelines execute without affecting each other.- Horizontally scalable, without a need to worry about sizing. As more dbt pipelines get added, there is little to no worry about - How it will affect existing pipeline executions.
- Billed only for the duration of the pipeline execution.
- Ability to execute multiple versions of the dbt data pipeline.
- Tagging of dbt data pipeline execution, so billing and cost analysis can be observed.
- Service accounts used by the dbt data pipeline can be different.
- Share the same cluster across multiple targets or multiple data sources.
- Logs are captured by cloud-native implementations.
- Notification and alerts activated based on logs.

Thus an implementation closer to dbt Cloud, minus the UI, running within your environment. The benefits of 
this approach lead to:
- The execution cluster can be maintained by an admin group, separate from the data pipeline development teams.
- The dbt data pipelines are developed and deployed by separate projects or development teams.
- You also want to run in an environment like Kubernetes, but your team has yet to learn and adopt Kubernetes.

Much of the capabilities mentioned above can be done using existing capabilities in the Cloud. 
Interested? Follow along as I walk through a prototype implementation using Azure Container Instance. 
I will present the various choices that were taken into consideration as well as other aspects of the 
code/functionalities.

If you are interested in an implementation for AWS, refer to my previous blog : [Deploying and Running dbt on AWS-Fargate](https://medium.com/hashmapinc/deploying-and-running-dbt-on-aws-fargate-872db84065e4). There are differences 
when it comes to Azure adoption vs AWS adoption, this is primarily due to the way how Azure Container 
Instance is different from AWS Fargate.

## Solution Overview

### Highlights
- A custom DBT docker image is hosted in Azure container registry (ACR).
- The dbt projects are packaged and hosted in an Azure file share.
- The service account, for logging into the data warehouse, is stored in Azure key vault.
- Each dbt model execution is wrapped in its own script file, Ex: [dbtdataops/dbtoncloud/dbt_datapipeline.sh].

 ![](./docs/images/dbt_on_aci.png)

Here are the execution steps:

1. A schedule trigger, like an Azure Function, starts a specific ACG.
2. This causes the underlying docker image to be pulled from the ACR.
3. Once pulled, an ACI is instantiated.
4. The instantiation would invoke the default configured script 'entrypoint.sh'. This script is just set by a working directory for the application.
5. The script 'entrypoint.sh' would then invoke 'run_pipeline.sh'.
6. The 'run_pipeline.sh', copies the dbt project hosted in a file share into its local temporary directory. The file share is mounted to the docker image. The dbt project, which is zip archived, is then unarchived.
7. The 'run_pipeline.sh' script would then instantiate a data pipeline specific script, 'dbt_datapipeline.sh', as configured in the environment variable ENV_DBT_RUN_CMD.
8. The data pipeline script script would invoke 'dbt_init_run.sh'
9. This 'dbt_init_run.sh' script uses a configured identity to access the key vault to reach the datastore connection information like url, userid, password etc..
10. A dbt model run is then performed. This would run the necessary transformation pipeline in the datastore (Snowflake or Big Query).

Once transformation is complete, the ACI gets Terminated.

### Orchestration with ADF 
  - [Trigger by ADF](./docs/ADF_Trigger.md)

## Design Considerations

### Choice of the execution environment
For a multi-tenant, horizontally scalable and almost serverless-like functionality, Kubernetes (AKS, EKS or GKE) would ideally be the initial thought. In my various client engagements, however, it was pretty evident that not all client teams were ready to take on Kubernetes. Some reasons were:

- Client was either in their initial journey into the cloud, they were not knowledgeable on Kubernetes.
- Most of the data acquisition, ingestion, processing etc were accomplished by various SAAS offerings, hence standing up a K8s cluster wass unnecessary.
- Hosting and running a k8s worker node 24/7 and performing only 1 hour of task for data processing does not make sense.

Azure Container Instance provides a simplistic approach to hosting and executing containerized applications. Adoption and implementation are much simpler and does allow the team to migrate the containers into Kubernetes in the future if needed.

The pricing of Azure Container Instance (ACI) was also preferred, as you are billed only for the duration of container execution. You don't need to have the container running 24/7. You can instantiate the container using APIs and once the container finishes its task it can be shutdown.

Scalability can be achieved by instantiating different instances of the ACI/ACG as needed. The instances differ primarily based on parameters passed to the ACI.

Security is offered if the underlying image can be pulled from the ACR and instantiated into an ACI.
All logs written to the console, by the container application, can be routed to Azure Monitor logs.

#### Why not Azure Functions?
Well long story short, Azure Functions has a current time limit of 15 minutes. While typically the data pipelines I had developed run less than 5 minutes, that timing would not be the same in all conditions.

The SQL complexity, DAG depth, and data volume might result in a longer execution time-limit. So, to be on the safe side, Azure Functions was not the choice.

### Azure Container Instance
Azure Container Instances (ACI) are packaged and deployed as Azure Container Groups (ACG). The ACG will host only 1 image of the dbt docker. This allows us to not have to worry about sizing. The ACG instance is specific to a dbt project. This allows isolation between multiple dbt projects.

Dbt is a simple process and the model transformation is delegated to the data warehouse, like Snowflake or BigQuery. For that reason, the memory and CPU requirement is very minimal, so the choice for CPU/memory is 1vCPU with 0.5GB.

### Docker Image
We expect the following functionalities to be present in the Docker image:
- DBT command/libraries and its dependencies
- Azure cli, with which we connect to resources like Azure file share, key vaults etc..
- Bash for shell script execution.

Azure provides azure cli as docker image, [mcr.microsoft.com/azure-cli](https://github.com/microsoft/containerregistry). This also contains the bash shell hence we use this as a base image. We also install DBT and push into Azure Container Registry (ACR).

#### What about the DBT project ?
A single DBT project typically can be executed with different model files. For example, a model run for data preparation would be seperate from a model run from materialization models.

Also an ACG instance is very specific to a DBT model execution. Azure currently does not allow running the same instance of ACG with different choices of parameters.

If you are going to ask, what about updating a container group using environment variables ?
[Doc: Update containers in Azure Container Instances](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-update)

The ACI gets auto terminated after a dbt model run. This allows us to reduce cost and keep the solution on an on-demand basis. However if you look at the below screenshot from the doc:

![](./docs/images/acg_update_note.png)
This "update" would not work for us. I tried & verified!!! Plus I was not convinced with the update command as I have to pass in the ACR credentials, cpu, memory and other parameters even though they are not meant to change.

Hence to keep the Docker image as independent as possible, the DBT project is not burnt into the image.

#### So how do we get the DBT project ?
There are multiple ways to achieve this. One way is Doc : Mount a gitRepo volume in Azure Container Instances. Another approach I achieved is:
- By packaging the DBT project into an Azure file share
- On ACG startup, downloading the project from the file share and unarchive the content.
![](./docs/images/dbt_code_hosted_file_share.png)

I choose this approach, as different clients have different code repos this allows us to adapt to any situation. Below is a screenshot of content of the dbt project that is archived.
![](./docs/images/dbtcloud_zip_content.png)

#### Why is the dbt data pipeline project not part of the docker image?
We wanted our solution to service multi-tenant. Preserving the dbt data pipeline projects into the docker image would result in multiple ECR's and costs could potentially go up.

By keeping the dbt data pipeline project outside of the image, we could have just one ECR instance, and less docker images to maintain from vulnerability scans. Technically, since the docker image does not have the client code, we host this image in the public docker hub too.

#### How does the container instance look?
![](./docs/images/docker_image_breakup.png)

#### What are the scripts that you keep mentioning?
From the time the container gets instantiated, to the time the dbt model execution occurs, a set of pre-developed scripts are executed. They are as follows:
- entrypoint.sh: This is present in the docker image. Its main purpose is to create an application directory to host the dbt project. It will then proceed to invoke the run_pipeline.sh script.
- run_pipeline.sh: This is used to unarchive the dbt project and invoke the specific dbt wrapper script, for ex: dbtdataops/dbtoncloud/dbt_datapipeline.sh.
- dbt_run_init.sh: This is used to read the Snowflake connection information from a key vault.

#### Why not dbt rpc?
Using Dbt rpc would require you to host dbt in a container and this container might need to be available and up 24/7. You would end up increasing the size of the container as it might need to process multiple dbt data pipelines in parallel.

## Observations
### Cost
During our multiple execution runs of sample projects, our billing incurred was < $2 USD/day. The mileage will vary in your environment when adopting this approach for wider projects.

### Execution time
As mentioned earlier, the execution time varies across scenarios. For example, based on the data volume a typical initial data load could run longer while incremental updates could be shorter.

### Logging
All logging can be sent to Azure Monitor Logs. This will be an item to be implemented based on your individual use case and requirements.

### AWS Fargate vs Azure Container Instance
If given a choice between AWS Fargate vs ACI; I would choose the AWS Fargate approach. The reason being that It allowed me to create a single instance template and use it widely across multiple projects and scale horizontally more. Part of the reason being that the flexibility of changing the environment variable during each run. However in ACI, changing the environment variable for the same instance is just not feasible. Especially for short lived ACI instances.
If you want dbt to be run inside your network; it's just a small configuration in the case of Fargate. Whereas, in Azure you might end up in a typical hosting situation as you would with other Azure resources.

## Final Thoughts
This pattern and reference implementation have been implemented based on a cloud Well-Architected framework. It might not address all the points, but is a start.
