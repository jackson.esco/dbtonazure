
variable "spsecret" {
  description = "The secret for the service principal used for container registry."
}

variable "module_name" {
  default = "dbtonaz"
}

variable "module_short_name" {
  default = "dbtaz"
}

variable "azregion" {
  default = "East US"
}

variable "azregion_short_code" {
  description = "region short code where resource is deployed"
  default     = "use1"
}

variable "tags_base" {
  default = {
    "created-by" = "venkat"
    "purpose"    = "Demonstrate dbt on azure container instance"
    "owner"      = "venkat"
  }
}

variable "rg_name" {
  default = "dbtonaz_rg"
}

variable "dbt_docker_tag" {
  default = "hmap/dbtonaz"
}
