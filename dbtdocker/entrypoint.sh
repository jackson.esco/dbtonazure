#!/bin/bash

#####################################################################################
# This script will get executed as part of the task execution. It does the following
#   - Creates a temp directory ($APP_DIR)
#   - Copies the content from S3 bucket into $APP_DIR
#   - Executes the script 'run_pipeline.sh'; found in the s3 bucket content
#####################################################################################

APP_DIR=/tmp/appdir

echo "================================="
echo "Subscription : $ENV_AZSUB"
echo "RG : $ENV_RG"
echo "Key vault : $ENV_KV_SFLK @ $ENV_KV_URL "
echo "Storage account : $ENV_CODE_SA"
echo "DBT project path : $ENV_DBT_PACKAGED"
echo "Run command : $ENV_DBT_RUN_CMD"

ls -lR /code

echo "========================================="
echo "Cleanup from previous run ..."
rm -rf $APP_DIR > /dev/null 2>&1

echo "Creating the workdir $APP_DIR ..."
mkdir -p $APP_DIR
cd $APP_DIR

echo "Copying dbt project artifact to workdir ..."
cp $ENV_DBT_PACKAGED .

unzip *.zip
ls -lR .

/run_pipeline.sh "$ENV_DBT_RUN_CMD"


echo " ------------------------------------- "
echo " Finished!!! "
